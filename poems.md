These are small poems; some of them are shaped in a kind of programming-like langugage rather than a normal human language. This is just the way that my thoughts wanted to go, so I let them.

This file is licensed under [cc by 4.0](https://creativecommons.org/licenses/by/4.0) license.

# English

## 001
    if (this test is true) {
        Are you sure?
    } else {
        I'm not.
    }

    repeat;


## 002
    please wait. Looking for something
    Error
    please wait. Looking for something

## 003
    self.undelete()

## 004
    ;; Elisp poem that you can actually run
    (defun say (words left)
      (unless (<= left 0)
        (let* ((which-word (random (length words)))
              (the-word-is (nth which-word words)))
          (and (insert
                (concat
                 " "
                 the-word-is)))
          (say words (- left 1)))))
    (and (say '("нема" "мене"
                "є" "немає"
                "ємен" "нємен")
              16))

## 005
    bottle.put(msg)
    bottle.send()

## 006
    This is not
    a poem
    about love

### 007
    my code
    is your code


# Ukrainian

## 001
    Мовлю:
    Я немовля

## 002
    п’ятниця, вечір
    ніч…ранок
    день…сутінки су-діля-ділок
    поне-вівторок
    середа!
    останній
    день
    літа-бо
    от-от

## 003
    клона мати
    не хочу
    хочу бути
    одним з трьох
    трійнят

## 004
    [зарезервовано]

## 005
    післямова:
    мов(би)чу(ти)(му)

## 006 (пере-напам'ять)
    а рима
    дверима
    хіп-
      (-хоп)

## 007
    не на ча́сі
    на  неча́сі
      неначасі́
           усі́
    (часові — вісь
           а мені?)
